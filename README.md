QcomLT DebugBoard
=================

[96boards specifications](https://www.96boards.org/specifications/) compliant Mezzanine used to control the DC Input, USB VBUS, PWR/RST buttons and Debug UART via a single USB2.0 CDC interface.

![KiCad 3D PCB preview!](/QcomLT_DebugBoard.png)

The current design has been saved from KiCad 7.

Firmware is available in [Firmware.zip](/Firmware.zip), Gerber zip file in [gerber/](/gerber/) directory and assembly files (BOM and positions) in [assembly/](/assembly/) directory.

License
=======

[CERN-OHL-P](/LICENSE) (CERN Open Hardware Licence v2 permissive)
